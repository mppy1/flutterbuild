
import 'dart:async';
import 'package:flutter/material.dart';
import 'news_details_page.dart';
import 'news_details_web.dart';

class TestHomeScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Title"),
      ),
      body: new Center(child: new Text("Click Me")),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        backgroundColor: Colors.orange,
        onPressed: () {
          print("Clicked");
          Navigator.push(context, new MaterialPageRoute(builder: (context) {
            return new NewsDetailsPage(
              25266.toInt(),
              detailsWeb: new DetailWebUse(),
            );
          }));
        },
      ),
    );
  }

}

class NewsApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: TestHomeScreen(),
    );
  }
}

class DetailWebUse implements DetailsWeb {
  NewsDetailsWeb web;

  @override
  Future<bool> canScrollDown() {
    return web?.canScrollDown();
  }

  @override
  Future<bool> canScrollUp() {
    return web?.canScrollUp();
  }

  @override
  Widget createHtmlWidget(String body, List<Widget> pageWidgetContainer) {
    web = new NewsDetailsWeb(
      body: body,
      widgets: pageWidgetContainer,
    );
    return web;
  }
}