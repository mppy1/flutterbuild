import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart' as provider;
import 'package:transparent_image/transparent_image.dart';

import '../model/counter_model.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final appName = 'Custom Themes';
    return new MaterialApp(
      title: appName,
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan[600],
      ),
      home: new MyHomePage(
        title: appName,
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, @required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _counter = provider.Provider.of<CounterModel>(context);
    final textSize = provider.Provider.of<int>(context).toDouble();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(title),
      ),
      body: SingleChildScrollView(
        child: new Center(
          child: new Container(
              color: Theme.of(context).accentColor,
              child: new Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Fluttertoast.showToast(
                          msg: 'test it',
                          toastLength: Toast.LENGTH_SHORT,
                          backgroundColor: Colors.grey[400]);
                    },
                    child: Container(
                      child: Text('测试'),
                    ),
                  ),
                  new Text(
                    'Text with a background color',
                    style: Theme.of(context).textTheme.title,
                  ),
                  new Text(
                    'Value: {$_counter.value}',
                    style: TextStyle(fontSize: textSize),
                  ),
                  new Image.network(
                    'http://attach.bbs.miui.com/forum/201401/11/145825zn1sxa8anrg11gt1.jpg',
                  ),
                  new Container(
                    color: Colors.white,
                    width: double.infinity,
                    height: 50,
                    child: Center(child: new CircularProgressIndicator())),
                  new FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image:
                          'http://attach.bbs.miui.com/forum/201111/21/205700txzuacubbcy91u99.jpg'),
                ],
              )),
        ),
      ),
      floatingActionButton: new Theme(
          data: Theme.of(context).copyWith(accentColor: Colors.yellow),
          child: new FloatingActionButton(
            onPressed: () {
              Fluttertoast.showToast(
                  msg: 'click it',
                  toastLength: Toast.LENGTH_SHORT,
                  backgroundColor: Colors.grey[400]);
            },
            child: new Icon(Icons.add),
          )),
    );
  }
}
